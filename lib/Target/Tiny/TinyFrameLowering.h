//==- TinyFrameLowering.h - Define frame lowering for Tiny --*- C++ -*--==//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
//
//
//===----------------------------------------------------------------------===//

#ifndef Tiny_FRAMEINFO_H
#define Tiny_FRAMEINFO_H

#include "Tiny.h"
#include "llvm/Target/TargetFrameLowering.h"

namespace llvm {
class TinyFrameLowering : public TargetFrameLowering {
protected:

public:
  /* -- argument description --
   * stack direction    -   stack grows down
   * stack allignment   -   allign to 4 bytes (registr size)
   * local area offset  -   offset of the local area from stack pointer when
   *                        entering function
   * transient stack allignment   -  more restrictive allignment (even during
   *                                 calls)
   */
  explicit TinyFrameLowering()
      : TargetFrameLowering(TargetFrameLowering::StackGrowsDown, 4, 0, 4) {}
   //   : TargetFrameLowering(TargetFrameLowering::StackGrowsDown, 4, -2, 2) {}

  /// emitProlog/emitEpilog - These methods insert prolog and epilog code into
  /// the function.
  void emitPrologue(MachineFunction &MF, MachineBasicBlock &MBB) const override;
  void emitEpilogue(MachineFunction &MF, MachineBasicBlock &MBB) const override;

  MachineBasicBlock::iterator
  eliminateCallFramePseudoInstr(MachineFunction &MF, MachineBasicBlock &MBB,
                                MachineBasicBlock::iterator I) const override;

  bool spillCalleeSavedRegisters(MachineBasicBlock &MBB,
                                 MachineBasicBlock::iterator MI,
                                 const std::vector<CalleeSavedInfo> &CSI,
                                 const TargetRegisterInfo *TRI) const override;
  bool restoreCalleeSavedRegisters(MachineBasicBlock &MBB,
                                  MachineBasicBlock::iterator MI,
                                  const std::vector<CalleeSavedInfo> &CSI,
                                  const TargetRegisterInfo *TRI) const override;

  bool hasFP(const MachineFunction &MF) const override;
  bool hasReservedCallFrame(const MachineFunction &MF) const override;
  void processFunctionBeforeFrameFinalized(MachineFunction &MF,
                                     RegScavenger *RS = nullptr) const override;

  bool assignCalleeSavedSpillSlots(
    MachineFunction &MF, const TargetRegisterInfo *TRI,
    std::vector<CalleeSavedInfo> &CSI) const;
};

} // End llvm namespace

#endif
