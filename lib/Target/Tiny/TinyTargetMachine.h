//===-- TinyTargetMachine.h - Define TargetMachine for Tiny -*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file declares the Tiny specific subclass of TargetMachine.
//
//===----------------------------------------------------------------------===//


#ifndef LLVM_TARGET_Tiny_TARGETMACHINE_H
#define LLVM_TARGET_Tiny_TARGETMACHINE_H

#include "TinySubtarget.h"
#include "llvm/Target/TargetFrameLowering.h"
#include "llvm/Target/TargetMachine.h"

namespace llvm {

/// TinyTargetMachine
///
class TinyTargetMachine : public LLVMTargetMachine {
  std::unique_ptr<TargetLoweringObjectFile> TLOF;
  TinySubtarget        Subtarget;

public:
  TinyTargetMachine(const Target &T, const Triple &TT,
                      StringRef CPU, StringRef FS, const TargetOptions &Options,
                      Optional<Reloc::Model> RM, CodeModel::Model CM,
                      CodeGenOpt::Level OL);

  const TinySubtarget *getSubtargetImpl(const Function &F) const override {
    return &Subtarget;
  }

  TargetPassConfig *createPassConfig(PassManagerBase &PM) override;

  TargetLoweringObjectFile *getObjFileLowering() const override {
    return TLOF.get();
  }
}; // TinyTargetMachine.

} // end namespace llvm

#endif // LLVM_TARGET_Tiny_TARGETMACHINE_H
