//==-- Tiny.h - Top-level interface for Tiny representation --*- C++ -*-==//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file contains the entry points for global functions defined in
// the LLVM Tiny backend.
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_TARGET_Tiny_H
#define LLVM_TARGET_Tiny_H

#include "MCTargetDesc/TinyMCTargetDesc.h"
#include "llvm/Target/TargetMachine.h"

namespace TinyCC {
  // Tiny specific condition code.
  enum CondCodes {
    COND_E  = 0,  // aka COND_Z
    COND_NE = 1,  // aka COND_NZ
    COND_HS = 2,  // aka COND_C
    COND_LO = 3,  // aka COND_NC
    COND_GE = 4,
    COND_L  = 5,

    COND_INVALID = -1
  };
}

namespace llvm {
  class TinyTargetMachine;
  class FunctionPass;
  class formatted_raw_ostream;

  FunctionPass *createTinyISelDag(TinyTargetMachine &TM,
                                    CodeGenOpt::Level OptLevel);

  FunctionPass *createTinyBranchSelectionPass();

} // end namespace llvm;

#endif
