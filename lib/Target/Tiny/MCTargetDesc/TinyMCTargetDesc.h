//===-- TinyMCTargetDesc.h - Tiny Target Descriptions -------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file provides Tiny specific target descriptions.
//
//===----------------------------------------------------------------------===//

#ifndef TinyMCTARGETDESC_H
#define TinyMCTARGETDESC_H

namespace llvm {
class Target;

extern Target TheTinyTarget;

} // End llvm namespace

// Defines symbolic names for Tiny registers.
// This defines a mapping from register name to register number.
#define GET_REGINFO_ENUM
#include "TinyGenRegisterInfo.inc"

// Defines symbolic names for the Tiny instructions.
#define GET_INSTRINFO_ENUM
#include "TinyGenInstrInfo.inc"

#define GET_SUBTARGETINFO_ENUM
#include "TinyGenSubtargetInfo.inc"

#endif
