//===-- TinyMCAsmInfo.cpp - Tiny asm properties -----------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file contains the declarations of the TinyMCAsmInfo properties.
//
//===----------------------------------------------------------------------===//

#include "TinyMCAsmInfo.h"
#include "llvm/ADT/StringRef.h"
using namespace llvm;

void TinyMCAsmInfo::anchor() { }

TinyMCAsmInfo::TinyMCAsmInfo(const Triple &TT) {
  PointerSize = CalleeSaveStackSlotSize = 4;

  CommentString = ";";

  AlignmentIsInBytes = false;
  //UsesELFSectionDirectiveForBSS = true;
}
