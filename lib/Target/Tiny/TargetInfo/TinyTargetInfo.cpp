//===-- TinyTargetInfo.cpp - Tiny Target Implementation ---------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

#include "Tiny.h"
#include "llvm/IR/Module.h"
#include "llvm/Support/TargetRegistry.h"
using namespace llvm;

Target llvm::TheTinyTarget;

extern "C" void LLVMInitializeTinyTargetInfo() {
  RegisterTarget<Triple::tiny>
    X(TheTinyTarget, "tiny", "Tiny [experimental]");
}
